//
// Created by Masahiko Tsujita on 2017/03/18.
//

#ifndef DISPATCHCPP_DISPATCHQUEUE_H
#define DISPATCHCPP_DISPATCHQUEUE_H

#include <list>
#include <queue>
#include <thread>
#include <mutex>
#include "RunLoop.h"

namespace dispatchcpp {

class DispatchQueue {
private:
    
    struct ScheduledDispatchObject {
        std::function<void(void)> func;
        std::chrono::system_clock::time_point scheduledAt;
    };
    
public:
    
    DispatchQueue(RunLoop &runLoop)
        : _mutex()
        , _runLoop(runLoop)
        , _functionQueue()
        , _sortedScheduledDispatchObjects()
    {
        _runLoop.addTimer(
            {
                std::chrono::system_clock::now(),
                true,
                std::chrono::milliseconds(1000/60),
                [&](auto...){
                    this->update();
                }
            }
        );
    }
    
    ~DispatchQueue() {
        _mutex.lock();
        _runLoop.stop();
        _mutex.unlock();
    }
    
    void update() {
        _mutex.lock();
        enqueueExpiredScheduledDispatchObjectsNoThreadSafe();
        _mutex.unlock();
        std::function<void(void)> f;
        bool stop = false;
        while (!stop) {
            _mutex.lock();
            if (_functionQueue.empty()) {
                stop = true;
            } else {
                f = std::move(_functionQueue.front());
                _functionQueue.pop();
            }
            _mutex.unlock();
            if (!stop) {
                f();
            }
        }
    }
    
    template<typename F>
    void async(F &&f) {
        _mutex.lock();
        _functionQueue.push(std::forward<std::function<void(void)>>(f));
        _mutex.unlock();
    }
    
    template<typename F>
    void asyncAfter(std::chrono::system_clock::duration &&duration, F &&f) {
        ScheduledDispatchObject dispatchObject {
            f,
            std::chrono::system_clock::now() + duration
        };
        _mutex.lock();
        auto at = std::lower_bound(
            _sortedScheduledDispatchObjects.cbegin(),
            _sortedScheduledDispatchObjects.cend(),
            dispatchObject,
            [](auto &&lhs, auto&& rhs){ return lhs.scheduledAt < rhs.scheduledAt; }
        );
        _sortedScheduledDispatchObjects.insert(at, dispatchObject);
        _mutex.unlock();
    }

private:
    
    void enqueueExpiredScheduledDispatchObjectsNoThreadSafe() {
        auto now = std::chrono::system_clock::now();
        while (!_sortedScheduledDispatchObjects.empty() && _sortedScheduledDispatchObjects.front().scheduledAt <= now) {
            auto& front = _sortedScheduledDispatchObjects.front();
            async(std::move(front.func));
            _sortedScheduledDispatchObjects.pop_front();
        }
    }
    
private:
    mutable std::recursive_mutex _mutex;
    RunLoop& _runLoop;
    std::queue<std::function<void(void)>> _functionQueue;
    std::list<ScheduledDispatchObject> _sortedScheduledDispatchObjects;
    
};

}

#endif //DISPATCHCPP_DISPATCHQUEUE_H
