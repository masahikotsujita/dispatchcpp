//
// Created by Masahiko Tsujita on 2017/04/01.
//

#ifndef DISPATCHCPP_RUNLOOPTIMER_H
#define DISPATCHCPP_RUNLOOPTIMER_H

#include <functional>
#include <chrono>

namespace dispatchcpp {

class RunLoopTimer final {
public:
    
    using Date = std::chrono::system_clock::time_point;
    using Interval = std::chrono::system_clock::duration;
    using Callback = std::function<void(RunLoopTimer &timer)>;
    
    template<typename TyCallback>
    RunLoopTimer(
        const Date &fireDate,
        bool repeats,
        const Interval &repeatInterval,
        TyCallback &&callback
    )
        : _nextFireDate(fireDate)
        , _doesRepeats(repeats)
        , _repeatInterval(repeatInterval)
        , _callback(callback)
        , _isValid(true)
    {
    }
    
    const Date &getNextFireDate() const {
        return _nextFireDate;
    }
    
    void setNextFireDate(const Date &nextFireDate) {
        _nextFireDate = nextFireDate;
    }
    
    bool doesRepeats() const {
        return _doesRepeats;
    }
    
    const Interval &getRepeatInterval() const {
        return _repeatInterval;
    }
    
    void setRepeatInterval(const Interval &repeatInterval) {
        _repeatInterval = repeatInterval;
    }
    
    void fire() {
        _callback(*this);
    }
    
    bool isValid() const {
        return _isValid;
    }
    
    void invalidate() {
        _isValid = false;
    }

private:
    
    Date _nextFireDate;
    bool _doesRepeats = false;
    Interval _repeatInterval;
    Callback _callback;
    bool _isValid;
    
};

}

#endif //DISPATCHCPP_RUNLOOPTIMER_H
