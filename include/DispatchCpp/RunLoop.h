//
// Created by Masahiko Tsujita on 2017/03/25.
//

#ifndef DISPATCHCPP_RUNLOOP_H
#define DISPATCHCPP_RUNLOOP_H

#include <list>
#include <thread>
#include <DispatchCpp/RunLoopTimer.h>

namespace dispatchcpp {

class RunLoop final {
public:
    
    RunLoop()
        : _isStopRequested(false)
        , _timers()
    {
    }
    
    void run() {
        _isStopRequested = false;
        while (!_isStopRequested && !_timers.empty()) {
            std::this_thread::sleep_until(_timers.front().getNextFireDate());
            const auto now = std::chrono::system_clock::now();
            while (!_timers.empty() && _timers.front().getNextFireDate() < now) {
                RunLoopTimer timer = std::move(_timers.front());
                _timers.pop_front();
                timer.fire();
                if (timer.isValid()) {
                    if (timer.doesRepeats()) {
                        timer.setNextFireDate(now + timer.getRepeatInterval());
                    }
                    addTimer(std::move(timer));
                }
            }
        }
        _isStopRequested = false;
    }
    
    void stop() {
        _isStopRequested = true;
    }
    
    void addTimer(RunLoopTimer&& timer) {
        auto at = std::lower_bound(
            _timers.begin(), _timers.end(),
            timer,
            [](const auto &lhs, const auto &rhs){
                return lhs.getNextFireDate() < rhs.getNextFireDate();
            }
        );
        _timers.insert(at, std::move(timer));
    }
    
private:
    RunLoop(RunLoop &&) = delete;
    RunLoop &operator = (RunLoop &&) = delete;
    
    bool _isStopRequested;
    std::list<RunLoopTimer> _timers;
    
};

}

#endif //DISPATCHCPP_RUNLOOP_H
