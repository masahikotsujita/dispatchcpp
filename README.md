# DispatchCpp

C++ concurrent task execution library inspired by Apple's [libdispatch](https://github.com/apple/swift-corelibs-libdispatch).
