//
// Created by Masahiko Tsujita on 2017/03/21.
//

#ifndef DISPATCHCPP_TESTUTILITY_H
#define DISPATCHCPP_TESTUTILITY_H

namespace dispatchcpp_tests {

namespace async_defaults {
constexpr auto timeout = std::chrono::seconds(5);
constexpr auto pollInterval = std::chrono::milliseconds(1000 / 60);
}

template<typename Condition>
bool waitEventually(
    Condition &&condition,
    std::chrono::system_clock::duration &&timeout = async_defaults::timeout,
    std::chrono::system_clock::duration &&pollInterval = async_defaults::pollInterval
) {
    auto timeoutAt = std::chrono::system_clock::now() + timeout;
    while (std::chrono::system_clock::now() < timeoutAt) {
        if (condition()) {
            return true;
        }
        std::this_thread::sleep_for(pollInterval);
    }
    return false;
}

}

#define ASSERT_EVENTUALLY(condition) \
    ASSERT_TRUE(dispatchcpp_tests::waitEventually([&]() -> bool { return condition; }))

#define ASSERT_EVENTUALLY_NOT(condition) \
    ASSERT_FALSE(dispatchcpp_tests::waitEventually([&]() -> bool { return condition; }))

#endif //DISPATCHCPP_TESTUTILITY_H
