//
// Created by Masahiko Tsujita on 2017/03/19.
//

#include <gtest/gtest.h>
#include <DispatchCpp/DispatchCpp.h>
#include "TestUtility.h"

using namespace dispatchcpp;

class DispatchQueueTest : public testing::Test {
protected:
    void SetUp() override {
        runLoop = std::make_shared<RunLoop>();
        queue = std::make_shared<DispatchQueue>(*runLoop.get());
        thread = std::make_shared<std::thread>([=]{
            runLoop->run();
        });
    }

    void TearDown() override {
        runLoop->stop();
        thread->join();
        queue = nullptr;
        runLoop = nullptr;
        thread = nullptr;
    }
    
    std::shared_ptr<DispatchQueue> queue;
    std::shared_ptr<RunLoop> runLoop;
    std::shared_ptr<std::thread> thread;
};

TEST_F(DispatchQueueTest, TestSetFlagAsync) {
    std::atomic_bool flag { false };
    queue->async([&]{
        flag = true;
    });
    ASSERT_EVENTUALLY(flag);
}

TEST_F(DispatchQueueTest, TestCountUpAsync) {
    const int count = 100;
    std::atomic_int counter { 0 };
    for (int i=0; i < count; ++i) {
        queue->async([&]{
            counter += 1;
        });
    }
    ASSERT_EVENTUALLY(counter == count);
}

TEST_F(DispatchQueueTest, TestAsyncAfter) {
    std::atomic_bool flag { false };
    queue->asyncAfter(std::chrono::seconds(3), [&]{
        flag = true;
    });
    ASSERT_EVENTUALLY(flag);
}

TEST_F(DispatchQueueTest, TestAsyncAfterTimeout) {
    std::atomic_bool flag { false };
    queue->asyncAfter(std::chrono::seconds(10), [&]{
        flag = true;
    });
    ASSERT_EVENTUALLY_NOT(flag);
}
